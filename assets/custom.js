$(document).ready(function() {
  ProductSizeGuideInit();
  MobileMenuInit();
})



function ProductSizeGuideInit() {
  var $size_guide_open = document.querySelector('[data-size-guide-open]');
  var $popup = document.querySelector('.size-guide-popup');

  if(typeof($size_guide_open) != 'undefined' && $size_guide_open != null){
    $size_guide_open.addEventListener('click', function() {
      $popup.classList.add('open');
    })
  }

  var $size_guide_close = document.querySelectorAll('[data-size-guide-close]');
  $size_guide_close.forEach(function($close) {
    $close.addEventListener('click', function() {
      $popup.classList.remove('open');
    })
  })

}

function MobileMenuInit() {
  if($(window).width() < 768) {
    $(document).on('click', '.sf-menu-links .sf-link button', function() {
      $(this).toggleClass('menu-open');
      $(this).parent().find('.sf-sub-links').slideToggle();
    })
  }
}